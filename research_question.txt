Group: group 56

Question
========

RQ: Is there a correlation between the age of the driver and the number of accidents occurred?

Null hypothesis: There is no correlation between the age of the driver and the number of accidents that occurred.

Alternative hypothesis: There is a correlation between the age of the driver and the number of accidents that occurred.

Dataset
=======

URL: https://www.kaggle.com/benoit72/uk-accidents-10-years-history-with-many-variables

Column Headings:

```                         
> Vehicles0514 <- read.csv("Vehicles0514.csv")
> colnames(Vehicles0514)
  [1] "ï..Accident_Index"                "Vehicle_Reference"                "Vehicle_Type"                    
  [4] "Towing_and_Articulation"          "Vehicle_Manoeuvre"                "Vehicle_Location.Restricted_Lane"
  [7] "Junction_Location"                "Skidding_and_Overturning"         "Hit_Object_in_Carriageway"       
  [10] "Vehicle_Leaving_Carriageway"      "Hit_Object_off_Carriageway"       "X1st_Point_of_Impact"            
  [13] "Was_Vehicle_Left_Hand_Drive."     "Journey_Purpose_of_Driver"        "Sex_of_Driver"                   
  [16] "Age_of_Driver"                    "Age_Band_of_Driver"               "Engine_Capacity_.CC."            
  [19] "Propulsion_Code"                  "Age_of_Vehicle"                   "Driver_IMD_Decile"               
  [22] "Driver_Home_Area_Type"
  
```